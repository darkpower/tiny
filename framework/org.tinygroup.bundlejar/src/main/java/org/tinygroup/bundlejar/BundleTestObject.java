package org.tinygroup.bundlejar;

import java.io.Serializable;

public class BundleTestObject implements Serializable{
	private String name;
	private int num;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
}
